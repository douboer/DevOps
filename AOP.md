
# AOP & IOC
---

## :red_circle: AOP

## :red_circle: IOC
IoC(Inversion of Control) 
![4236553-c415d1c39ed74772.png](https://i.loli.net/2020/05/13/uDnFJUXYAlSogh4.png)
- DL 已经被抛弃，因为他需要用户自己去是使用 API 进行查找资源和组装对象。即有侵入性。
- DI 是 Spring 使用的方式，容器负责组件的装配。

![u_4258379052,1368074021_fm_26_gp_0.png](https://i.loli.net/2020/05/13/kSFnAjGUgK2YeMZ.png)
![timg.jpg](https://i.loli.net/2020/05/13/SOxPgI3up25tbv8.jpg)

**疑问：**
- 通过xml配置或容器方式生成实例替代调用关系生成实例，解耦没错，怎么叫反转？A要用到B类，需要实例化B，IOC则是把实例化过程交给容器，A只管使用，不管实例化过程。

## :red_circle: 一些概念
### java 注解(Annotation) & python 装饰器(decorator)
python这里的装饰器是一个有逻辑的，可以执行的函数，只不过其写法有些特殊要求；
Java里面的Annotation只是个标记，需要其他代码来“根据标记执行“。

### POJO (Plain Ordinary Java Object) &  java bean
POJO是一个简单的普通的Java对象，它不包含业务逻辑或持久逻辑等，但不是JavaBean、EntityBean等，不具有任何特殊角色和不继承或不实现任何其它Java框架的类或接口。具有一部分getter/setter方法的那种类就可以称作POJO。


## :red_circle: references
- [IOC概念详解](https://www.jianshu.com/p/745a17f519a6)
- [IOC Spring揭秘阅读总结](https://www.jianshu.com/p/39f15ced9a74)
- [什么是面向切面编程AOP？](https://www.zhihu.com/question/24863332)


-- 20200514 复习

