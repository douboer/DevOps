
# SDN

## vpp
- [基于VPP+DPDK开源框架开发UPF](https://workerwork.github.io/posts/vpp/)


## reference
- [性能之殇（五）-- DPDK、SDN 与大页内存](https://lvwenhan.com/%E6%93%8D%E4%BD%9C%E7%B3%BB%E7%BB%9F/496.html)
- [半虚拟化技术 - VIRTIO 简介](http://tinylab.org/virtio-intro/)
- [virtio+ovs转发原理和性能分析](https://blog.csdn.net/qihoo_tech/article/details/112386368)
- [ovs实践--openFlow跨网段组网](https://segmentfault.com/a/1190000019612525?utm_source=tag-newest)
- [SDN网络指南](https://www.bookstack.cn/read/sdn-handbook/SUMMARY.md)
- [SDN和白盒交换机漫谈](https://www.sdnlab.com/19845.html)
- [浅析基于DPDK框架下OVS与VPP的差异点](https://www.secrss.com/articles/14352)
- []()
- []()
