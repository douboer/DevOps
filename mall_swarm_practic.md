
# mall-swarm 容器化实践

```
# docker & docker-compose安装
yum install docker-ce
[root@rancher01 ~]# curl -L https://github.com/docker/compose/releases/download/2.0.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
[root@rancher01 ~]# chmod +x /usr/local/bin/docker-compose

# ERROR
09:37:54 ~] docker-compose --version
/usr/local/bin/docker-compose: line 1: Not: command not found

# SOLVE, 用pip安装
https://blog.csdn.net/Deeeelete/article/details/103496918
```

```
yum install python
[root@rancher01 ~]# pip3 install docker-compose

[root@rancher01 ~]# docker-compose --version
docker-compose version 1.29.2, build unknown
```

```
#须在root下运行
[root@rancher01 mall-swarm.git]# docker-compose -f document/docker/docker-compose-env.yml up -d
。。。
Creating elasticsearch ...
Creating mysql         ...
Creating rabbitmq      ...
Creating nacos-registry ...
Creating elasticsearch  ... done
Creating mysql          ... done
Creating rabbitmq       ... done
Creating nacos-registry ... done
Creating mongo          ... done
ERROR: for nginx  Cannot start service nginx: driver failed programming external connectivity on endpoint nginx (ade4f1a99b1c7fef74e68ba68ca8bb1f2e663e5411eeed52c6d54539cb
Creating redis          ... done
Creating kibana         ... done
Creating logstash       ... error

ERROR: for logstash  Cannot start service logstash: OCI runtime create failed: container_linux.go:380: starting container process caused: process_linux.go:545: container init caused: rootfs_linux.go:76: mounting "/mydata/logstash/logstash.conf" to rootfs at "/usr/share/logstash/pipeline/logstash.conf" caused: mount through procfd: not a directory: unknown: Are you trying to mount a directory onto a file (or vice-versa)? Check if the specified host path exists and is the expected type

ERROR: for nginx  Cannot start service nginx: driver failed programming external connectivity on endpoint nginx (ade4f1a99b1c7fef74e68ba68ca8bb1f2e663e5411eeed52c6d54539cbadfdad): Bind for 0.0.0.0:80 failed: port is already allocated

ERROR: for logstash  Cannot start service logstash: OCI runtime create failed: container_linux.go:380: starting container process caused: process_linux.go:545: container init caused: rootfs_linux.go:76: mounting "/mydata/logstash/logstash.conf" to rootfs at "/usr/share/logstash/pipeline/logstash.conf" caused: mount through procfd: not a directory: unknown: Are you trying to mount a directory onto a file (or vice-versa)? Check if the specified host path exists and is the expected type
ERROR: Encountered errors while bringing up the project.

# SOLVE
按http://www.macrozheng.com/#/deploy/mall_deploy_docker_compose操作解决 logstash启动问题
```


```
mysql> grant all privileges on *.* to 'reader' @'%' identified by 'awifi@123';

[root@rancher01 docker]# docker run --name logstash -p 4560:4560 -p 4561:4561 -p 4562:4562 -p 4563:4563 \
> --link elasticsearch:es \
> -v /mydata/logstash/logstash.conf:/usr/share/logstash/pipeline/logstash.conf \
> -d logstash:7.6.2
docker: Error response from daemon: Conflict. The container name "/logstash" is already in use by container "39f1c67063ea455bfa235bbe98be4e90bd87c2f7864d856f0a7b17c32ae75fa0". You have to remove (or rename) that container to be able to reuse that name.
See 'docker run --help'.

# SOLVE
docker container rm logstash
#再运行
[root@rancher01 docker]# docker run --name logstash -p 4560:4560 -p 4561:4561 -p 4562:4562 -p 4563:4563 --link elasticsearch:es -v /mydata/logstash/logstash.conf:/usr/share/logstash/pipeline/logstash.conf -d logstash:7.6.2
eb420d0f7cc1e8b5506dbbdf9d4805b689c1795974b4f1f9e667f939b45d7378
docker: Error response from daemon: Cannot link to /elasticsearch, as it does not belong to the default network.

#SOLVE
[root@rancher01 docker]# docker network ls
NETWORK ID     NAME             DRIVER    SCOPE
ae8a541790cb   bridge           bridge    local
62bb5b73670c   docker_default   bridge    local
8033b07ea188   host             host      local
53fc0e8376f3   none             null      local

[root@rancher01 docker]# docker container rm logstash
logstash
[root@rancher01 docker]# docker run --name logstash -p 4560:4560 -p 4561:4561 -p 4562:4562 -p 4563:4563 --link elasticsearch:es --net docker_default -v /mydata/logstash/logstash.conf:/usr/share/logstash/pipeline/logstash.conf -d logstash:7.6.2
13f428ca4156d923a46cde6e01a8ab4569bc7e76e71776ab7431eed4e2fe2a2a
```

```
docker ps | grep logstash
发现logstash容器没有起来，配置不对

[root@rancher01 elk]# cp /home/gavin/mall-swarm.git/document/elk/logstash.conf /mydata/logstash/
[root@rancher01 docker]# docker container rm logstash
logstash
[root@rancher01 docker]# docker run --name logstash -p 4560:4560 -p 4561:4561 -p 4562:4562 -p 4563:4563 --link elasticsearch:es --net docker_default -v /mydata/logstash/logstash.conf:/usr/share/logstash/pipeline/logstash.conf -d logstash:7.6.2
13f428ca4156d923a46cde6e01a8ab4569bc7e76e71776ab7431eed4e2fe2a2a

# 进入容器使用如下命令安装插件
[root@rancher01 elk]# docker exec -it logstash /bin/bash
logstash-plugin install logstash-codec-json_lines
```

## reference
- [ELK 日志分析实例](https://my.oschina.net/magedu/blog/693876)


