
# 监控技术

zabbix + grafana
prometheus + grafana

**Thanos**

我们需要在TKE集群部署Prometheus主动采集workload负载指标和用户自定义指标，随着集群规模不断扩大，Promethues不支持容灾部署，不支持分布式，单实例容量瓶颈等问题也凸显出来，最后我们放弃了原生的Prometheus，转而使用Thanos（灭霸）实现了分布式、高可用容灾部署和数据长期存储。

Thanos Query 可以对数据进行聚合与去重，所以可以很轻松实现高可用：相同的 Prometheus 部署多个副本(都附带 Sidecar)，然后 Thanos Query 去所有 Sidecar 查数据，即便有一个 Prometheus 实例挂掉过一段时间，数据聚合与去重后仍然能得到完整数据。
基于Thanos，我们业务平台实现了高并发、海量的数据采集上报和存储。首先，因为所有流量都会经过网关，Thanos主动采集网关的这些指标到并将其可视化。如下图所示，只要服务接入了业务平台， QPS、耗时、成功率等一目了然，这些指标都无需额外开发即自动获得，对代码0侵入，节省了大量的开发成本。

[云原生背景下的运维价值思考与实践](https://cloud.tencent.com/developer/article/1753976)


