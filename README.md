# DevOps

## [内容清单*持续更新*](content.md)
## [运维知识体系](opsknowledge.md)

## [精益 & TPS](tps.md)
* JIT & Jidoka
 
## Load balance & HA
* [LB&HA笔记](hanote.md)

## mutiple sites disaster recovery
* [多活/异地容灾](disaster_recovery.md)

## 架构
* [taobao架构演进note](taobao_arch.md)

## 数据库
* [myCat笔记](mycat.md)
* [数据库中间件](dbmiddleware.md)
* [数据库](database.md)

## 缓存
* [redis]()
* [memcached](https://github.com/memcached/memcached)

## [微服务](microservice.md)
* Hystrix 断路器 熔断器

## 消息系统
* kafka
* rocketMQ

## 日志&监控
* flume
* openTSDB
* ELK
* sentry

## 统一认证
* ldap
* CAS做SSO

## 自动化运维
* [AIOps](aiops.md)
* [平台](platform.md)

## 持续集成CI/CD
* [发布管理](发布管理.md)

## 自动化测试

## 混沌工程 & 故障演练
* chaosblade
* chaosmonkey
* monkeyking

## 所谓中台
* [中台笔记](zhongtai.md)

## 研发创新
* [研发创新](chuangxin.md)

## 工具
* chef
* puppet
* docker
* Splunk
* k8s
* ansible

## 信息安全体系
* [信息安全](security.md)

## references
* [敏捷宣言](https://app.yinxiang.com/fx/69dc6058-4be9-4429-8766-b7d339e353b7)
* [监控知识体系](https://app.yinxiang.com/fx/77225f1a-3b6a-40ad-9733-e525aa406557)
* [如何为企业快速设计高可用的阿里云架构](https://app.yinxiang.com/fx/55cf9181-71c8-4c3b-9eaa-fc036cbdac8b)
* [myCat github docs](https://github.com/MyCATApache/Mycat-doc)
* [LDAP概念和原理介绍](https://www.cnblogs.com/wilburxu/p/9174353.html)
* [什么是Sentinel?它能做什么](https://blog.csdn.net/u012190514/article/details/81383698)
* [华为架构师8年经验谈：从单体架构到微服务的服务化演进之路](https://sdk.cn/news/4033)

* [ali Chaosblade](https://github.com/chaosblade-io/chaosblade/blob/master/README_CN.md)

* [关注价值流](https://cloud.tencent.com/developer/article/1373912)
* [Devops原则](https://www.pianshen.com/article/6683819777/)
* [每日构建与持续集成联系与区别](https://www.cnblogs.com/ailisatest/p/6253455.html)
* [你从未见过的五个强大的DevOps指标 ](https://www.sohu.com/a/251990665_711529)
* []()

## [运维问题](opsquestion.md)

