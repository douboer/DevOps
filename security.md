
# 信息安全体系

## [安全服务项目](sec_service.md)


2020/11/3 二大楼 安全会议 陈珂
-----------------------------------
1. 工具, 同步机制
2. 漏洞，3天修复率100%，对系统的影响
   整改完，一天2000条
3. 白名单
4. 通报前3天取数(一般26日)
   月报在1号2号取数

   每个月22号取数, 节假日提前到周五
   1号

   测试使用，不能修复漏洞说明

5. ip+port 

alpha,beta环境扫描工作,加白名单，减少暴露面

6. 暴露面
必须开放的暴露面清单
交换机
开放端口申请, 审核报告
**只要安全评估材料**，核验

<<中国电信179号文>>

7. 玄武盾平台, 我们的二级域名!!可以纳入

8. EDR, 青藤云, 漏洞+弱口令

9. fangtian - 暴露面 责任书 资产管理 核验材料
   houjunmeng - 备案 4A
   sunfansong - 4A
   chenke - 漏洞
   qianlv - agent

10. 云NAS  资产归宿问题

11. WIFI遗留问题，扫描后重新报备

12. 4A管理，机器导入


## 巡视安全整改举措
从反思问题成因、制定立行立改 目标举措时间点、 举一反三长效机制目标举措时间点等方面填写相关内容，并请明确相关联系人。同时将管局中期检查中主要扣分点纳入相关专业举一反三工作内容，管局中期检查结果详见附件。

认识不到位，动作落实迟缓。

加强安全管理队伍建设，专职人员自有编制4人，安全合作商一个。人员陆续到位2人，招聘人员因为招聘难，还未完全到位。

针对安全漏洞进行漏洞分类，针对不同分类的情况，布置落实漏洞修补的计划。其中98%属于私网漏洞，私网漏洞中5%属于自已代码研发漏洞。但是一些底层漏洞修补会带来代码需要修正，从而影响业务的正常运行，所以我们已经有初步计划，针对暴露面的漏洞及时限时修复，针对私网漏洞与业务的耦合关系安排具体的漏洞修复实施计划。

紧密跟踪漏洞的情况发布，加强自有漏洞主动发现能力，及时修补漏洞。加强自已系统的扫描频度和能力。

加强网络安全培训，落实安全考核。优化软件架构设计、优化自有代码研发、优化落实安全制度、优化落实操作安全动作。


## 2020/11/27 元华7007会议室
1. 漏洞&弱口令&暴露面修复
  - 11月份修复2468个漏洞
  - 修复弱口令48个
  - 互联网暴露面下降59个
2. 安全意识培训，漏洞提早检测、高频检测，口令设置强度意识
3. 自研项目架构优化，减少不必要的暴露面，提早升级开源软件版本

三个同步
暴露面&安全漏洞
账户三个月未使用的，冻结，封存

安全团队组织
安全质量
安全防护手段

2021年网信安建设规划


## 20201208 双新安全 会议
------------------------------
1. 双新(新业务，新技术，新产品，新平台等)评估问题
  - 云上，互联网，针对公众用户，有暴露面
  - “天翼云课堂”&“号百呼叫中心”
  - 定期核查，风险台账表格不完整
  - 部分业务风险识别

  - 核查核验口径
     范围: 除了前一年完成评估的业务，包括近三年
           历史评估清单纳入
  - 评估清单
     所有新业务
  - 报告

2. 线上网信安检查

3. 共性问题
  - 风险发现和整改
  - 未漏洞扫描报告
  - 自行减少报告目录中项目
  - 无架构图和无业务功能描述
  安恒配合沟通联系

4. 业务清单
  - 天翼看家
  - 云NAS
  - aWiFi业务平台
  - ? First专网
  - ? 小翼管楼
    - 没有评估查到扣分

## 20201214 工信部通报
- 平台、能力、人才
- 问责

责任. 从能力角度说
人员配置、平台、人才

举一反三动作：
- 提高网信安意识，思想重视，开全体会议
- 能力建设，探索全生命周期管控
- 人才培养
看看


