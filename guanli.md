
# 管理工作

## 轮岗制度

### 目的
- 轮岗目的有利于远程成长角度
  - 比如对运维知识的了解，对安全运维的了解，对自动化运维平台的了解，对业务的了解
  - 多岗锻炼，培养人才：通过内部的岗位轮换，开拓人才培养机制，尽快培养员工成长一专多能的复合型人才；
  - 激励员工，提高积极性：使员工在轮岗中开拓视野，锻造多方面的能力与经验，避免“岗位疲劳”;

- 规避风险，防止腐败：通过定期轮岗制度防范管理风险、道德风险、法律风险，以及形成腐败;
- 当企业处于旺季时，出现员工生病或者是请假，其他人员能很快顶上，不至于影响企业正常运转，或者是说当有人员离职时，企业可以马上有合适的人员顶上。

### 误解
- 企业是不是想要干掉我，所以开始培养接替我的人员了？才实行轮岗制。
- 如果企业实行了轮岗制，那么我岂不是变成一个可有可无，随时有被替换掉的人？
- 平时有人给我发发红包什么的，一旦实行轮岗制，那么对方岂不是知道了这么一回事？

### 注意点
- 必须要具备一套完整的科学管理制度，科学的轮岗管理制度，是**轮岗有序**，合理运行的保障，这里面涉及到详细的**轮岗计划**，让轮岗工作顺利有序的开展。
- 制定详细的轮岗计划，要根据企业的轮岗目标，制定详细的轮岗计划，对轮岗的**人数、时间、岗位**等要有详细的计划。
有目的，有计划，分步骤实施吗，做到岗位工作交接好，有人管，有人跟踪，有人评价。
- 明确轮岗资格，**轮岗岗位、轮岗对象**。哪些岗位需要轮岗，如何轮岗，为什么轮岗，轮岗后做什么？都要有明确的**轮岗岗位和部门**，避免漫无目的的轮岗。
- 明确轮岗的去向，轮岗人数的比例以及轮岗期限。**避免大面积轮岗**，确保正常工作的开展。
- **完善工作交接**，轮岗前要将工作中的物品交接、制度文件交接，工作任务详细内容进行交接，交接时，要有上级参与被移交者全程交接，并在交接工程中询问工作进度，工作需要注意事项等，一一罗列出来，使接手者有思想准备。
- 强化沟通、规范流程。在轮岗中，会出现一些问题与困难，人力资源部要全程跟踪，进行协调与沟通，同时人力资源部与部门负责人要与轮岗人员说明轮岗的意义，让他们理解轮岗的意义，消除内心的顾虑。

### 管理制度
- 轮岗由**谁统筹**，轮岗制执行、落实、监督、考核、由谁负责，这些就是要明确职责与权限，具体落实到人
- 轮岗涉及到的**对象**，哪些岗位需要进行轮岗，把岗位写出来
- 轮岗涉及到的**部门**，哪些部门需要轮岗，把部门写出来
- **轮岗期限**，如：科室内部轮岗期限1个月，部门内部轮岗期限2个月，跨部门轮岗期限半个月
- 轮岗**负责人**
  - 组内轮岗，由组长领导负责
  - 部门内部轮岗，由部门负责人负责
  - 跨部门轮岗，由统筹部门统一安排和实施
- 轮岗**计划**
  - 初步一年2次，制定员工轮岗计划：轮岗**人员名单，轮岗岗位，时间安排**
- 轮岗前工作移交，在轮岗前一周，必须进行工作移交，工作移交内容包括：
  - 完整的工作文件移交
  - 目前工作进展移交，包括工作进展程度，目标结果以及工作存在的障碍
  - 工作资源移交，客户资料、技术资料、采购资料等

---

### 领导审核

