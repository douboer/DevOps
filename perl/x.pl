use strict;
use warnings;
 
while(<>){
	chomp;
  print "$1'|\n" if /(errorCode.{5,6}\d*)/;
}

#\"errorCode\": \"000\"
#\"errorCode\":\"0\"
#\"errorCode\":\"vss_error_003\"

#cat zuul-log.txt | perl -ne "print \"$1\n\" if /(errorCode.{5,6}\d*)/"  | sort |uniq
#cat zuul-log.txt | perl -ne "print \"$1-$2\n\" if /(errorCode.+?\".+?\")(.+?)\\/"
#cat zuul-log.txt | perl -ne "print \"$1-$2\n\" if /(errorCode.+?\".+?\")(.+?)\\/"
#cat zuul-log.txt | perl -ne "print \"$1 : $2\n\" if /(errorCode).+?\".+?\"(.+?)\\/" | sort | uniq

## MAC OS
#cat zuul-log.txt | perl -ne 'print "$1 : $2\n" if /(errorCode).+?\".+?\"(.+?)\\/' | sort | uniq

# .+?  - ? 非贪婪
# 1 符号含义
# 2 \n 换行
# 3 \r 回车
# 4 \t 制表符
# 5 \f formfeed
# 6 \b 退格
# 7 \a 响铃
# 8 \e escape（ASCII 中的escape 字符）
# 9 \007 任何八进制值（这里是，007=bell(响铃)）
#10 \x7f 任何十六进制值（这里是，007=bell）
#11 \cC 一个控制符（这里是，ctrl +c）
#12 \\ 反斜线
#13 \” 双引号
#14 \l 下个字符小写
#15 \L 接着的字符均小写直到\E
#16 \u 下个字符大写
#17 \U 接着的字符均大写直到\E
#18 \Q 在non-word 字符前加上\，直到\E
#19 \E 结束\L,\U 和\Q
