"=============================================================================
"     FileName: _vimrc
"         Desc: _vimrc/_gvimrc for windows
"       Author: Chen Gan
"        Email: gavin.chan.hz@gmail.com
"     HomePage: xxxx.xxx.net/gavin
"      Version: 0.0.1
"   LastChange: 2014-01-17 09:07:16
"      History:
"               add VAM (vim addon management) - 2014/1/11                                                                  "
"=============================================================================

"
" echo "loading _vimrc ... "
"

source $VIMRUNTIME/vimrc_example.vim
source $VIMRUNTIME/mswin.vim
behave mswin
"colorscheme zellner
colorscheme desert

"
" 中文编码
" 同时支持GBK和UTF-8编码
"
set fileencodings=ucs-bom,utf-8,cp936
set fileencoding=utf-8
"set encoding=cp936 
set encoding=utf-8

"
" font of display
"
set guifont=Lucida_Console:h15:cANSI

"
"
" 启动最大化
"
winsize 1024 768
"au GUIEnter * simalt ~x
"colorscheme desert

"
"自动缩进
"
set autoindent

"
"行间距
"
set linespace=2

"
"设置tab制表符为4个空格
"
set ts=2
set expandtab
set shiftwidth=2
set cinoptions=>4,e0,n0,f0,{0,}0,^0,:s,=s,l0,gs,hs,ps,ts,+s,c3,C0,(2s,us,U0,w0,m0,j0,)20,*30

"
"set cindent
"
if has("autocmd")
  autocmd FileType html,jsp,js,xml set ts=2
  autocmd FileType html,jsp,js,xml set et
  autocmd FileType html,jsp,js,xml set shiftwidth=2
  autocmd FileType html,jsp,js,xml set cinoptions=>2,e0,n0,f0,{0,}0,^0,:s,=s,l0,gs,hs,ps,ts,+s,c3,C0,(2s,us,U0,w0,m0,j0,)20,*30
endif

" show filetypes in menu
"let do_syntax_sel_menu = 1 | runtime! synmenu.vim | aunmenu &Syntax.&Show\ filetypes\ in\ menu

"
" set syntax type
"
syntax on
"if &filetype != 'javacc'
"    setlocal filetype=javacc
"endif
"set syntax=javacc
"cal SetSyn("cpp")
"cal SetSyn("vb")
"cal SetSyn("perl")
"cal SetSyn("awk")

"
" write backup file (*~) to c:\tmp
"
"set nobackup
set undodir=~/tmp
set backupdir=~/tmp
set backup
setlocal noswapfile
"
" no beeps & no visible bell
"
set vb t_vb=

"
" show line number
"
set number

"
"hide toolbar
"see :help 'guioptions'
"
set guioptions-=T
"set guioptions-=m

set statusline=%F%m%r%h%w\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [ASCII=\%03.3b]\ [HEX=\%02.2B]\ [POS=%04l,%04v][%p%%]\ [LEN=%L]
set laststatus=2 " always show the status line

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"除了选项 textwidth 外，选项 formatoptions 确定了跟文本格式化有关的基本选项，常用的数值有：
"    * t：根据 textwidth 自动折行；
"    * c：在（程序源代码中的）注释中自动折行，插入合适的注释起始字符；
"    * r：插入模式下在注释中键入回车时，插入合适的注释起始字符；
"    * q：允许使用“gq”命令对注释进行格式化；
"    * n：识别编号列表，编号行的下一行的缩进由数字后的空白决定（与“2”冲突，需要“autoindent”）；
"    * 2：使用一段的第二行的缩进来格式化文本；
"    * l：在当前行长度超过 textwidth 时，不自动重新格式化；
"    * m：在多字节字符处可以折行，对中文特别有效（否则只在空白字符处折行）；
"    * M：在拼接两行时（重新格式化，或者是手工使用“J”命令），如果前一行的结尾或后一行的开头是多字节字符，则不插入空格，非常适合中文
"Vim 中 formatoptions 的缺省值是“tcq”，一般我会在 .vimrc 文件中加入一行“set formatoptions+=mM”
"来确保 Vim 能在中文字符之间折行而不要求空格的存在，并且在大部分情况下可以正确地处理中文重新格式化。
"
"
"回车自动添加注释符问题
" vimhome/ftpplugin/java.vim & javascript.vim
" "Set 'formatoptions' to break comment lines but not other lines,
" "and insert the comment leader when hitting <CR> or using "o".
" 修改以下这句
" "setlocal formatoptions-=t formatoptions+=croql
"set formatoptions=tq
"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

map <C-tab> gt
map <C-left> gT
map <C-right> gt

"perl settings
"<F7> for perl debugging
map <F7> :w<CR>:!perl -wd "%"<CR>
"<F8> for perl syntax checking (autosave first)
map <F8> :w<CR>:!perl -wc "%"<CR>
"<F9> to run by perl (autosave first) "",'' are both ok
map <F9> :w<CR>:!perl "%"<CR>

set complete-=i

" 禁用自动注释
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

"开始使用Vundle的必须配置
""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""
set nocompatible
filetype off
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

"使用Vundle来管理Vundle
"dos install command : vi +BundleInstall +qall
""""""""""""""""""""""""""""""""""""""""""""""""""""""
Bundle 'gmarik/vundle'
"PowerLine插件 状态栏增强展示
Bundle 'L9'
"Bundle 'Lokaltog/vim-powerline'
"Bundle 'mattn/emmet-vim'
"Bundle 'vim-scripts/AuthorInfo'
"Bundle 'Shougo/neocomplcache'
"Bundle 'FuzzyFinder'
"Bundle 'edsono/vim-matchit'
"Bundle 'The-NERD-tree'
"Bundle 'The-NERD-Commenter'
"Ag searcher
"Bundle 'rking/ag.vim'
"Bundle 'scrooloose/syntastic'
"Bundle 'TeTrIs.vim'
"Bundle 'vim-scripts/calendar.vim--Matsumoto'
"Bundle 'mbriggs/mark.vim'
"Bundle 'skammer/vim-css-color'
"Bundle 'vim-scripts/css3-mod'
Bundle 'ybian/smartim'

"Vundle配置必须 开启插件
filetype plugin indent on

""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:smartim_default = 'com.apple.keylayout.US'
let g:smartim_disable = 1

""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Bundle config
""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Vim-powerline config
""""""""""""""""""""""""""""""""""""""""""""""""""""""
"vim有一个状态栏 加上powline则有两个状态栏
set laststatus=2
set t_Co=256
let g:Powline_symbols='fancy'

"emmet-vim config
""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:user_emmet_expandabbr_key='<c-e>'
"let g:user_emmet_expandabbr_key='<Leader>e'
let g:user_emmet_complete_tag=1
let g:user_emmet_next_key='<c-n>'
let g:user_emmet_prev_key='<c-p>'

"authorinfo
let g:vimrc_author='Chen Gan'
let g:vimrc_email='gavin.chan.hz@gmail.com'
let g:vimrc_homepage='xxxx.xxx.net/gavin'
"???  error - E492
"通过bundle安装好后，修改C:\Users\gavin\.vim\bundle\AuthorInfo文件名ftpplugin 为plugin，solved 
nmap <F4> :AuthorInfoDetect<cr> 

"neocomplcache config
let g:neocomplcache_enable_at_startup = 1
let g:neocomplcache_force_overwrite_completefunc = 1
let g:neocomplcache_enable_ignore_case=1
" set the max list in the popup menu. increase the speed
let g:neocomplcache_max_list=20
inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"

"NERDTree
let NERDChristmasTree=1
let NERDTreeAutoCenter=1
let NERDTreeMouseMode=2
let NERDTreeShowBookmarks=1
let NERDTreeShowFiles=1
let NERDTreeShowHidden=0
let NERDTreeShowLineNumbers=1
let NERDTreeWinPos='left'
let NERDTreeWinSize=40
let NERDTreeIgnore=['\.vim$', '\~$', '\.o$', '\.d$', '\.a$']
let mapleader="," 
"输入,n或者按F9时打开/关闭NERDTree
nnoremap <silent> <Leader>n  :NERDTreeToggle <CR> 
nmap <silent> <F9> :NERDTreeToggle <cr>

"calendar-vim
"hotkey <leader>cal,水平方向：<leader>caL
let g:calendar_diary = "E:/gavin/note"  "设置日记的存储路径
let g:calendar_monday = 1               "以星期一为开始
let g:calendar_focus_today = 1          "光标在当天的日期上
"let g:calendar_mark = 'left-fit'       "可以让*和数字可靠近
let g:calendar_mark = 'right'           "上面设置后在昨天写日志，修改成right正常
"let g:calendar_mruler = '一月,二月,三月,四月,五月,六月,七月,八月,九月,十月,十一月,十二月' " 中文，可自行修改
"let g:calendar_wruler = '日 一 二 三 四 五 六'
"let g:calendar_navi_label = '往前,今日,往后'
let g:calendar_datetime = 'statusline' " can set 'title', 'statusline', '' for this option.

"mark
"hotkey <leader>m   -- mark
"       <leader>mc  -- clear mark
nmap <unique> <silent> <Leader>mc <Plug>MarkClear

"syntastic
let g:syntastic_check_on_open=1
"let g:syntastic_phpcs_conf='--tab-width=4 --standard=CodeIgniter'
"let makeprg='php -l -d error_reporting=E_ALL -d display_errors=1'

" css3_mod
let g:css_default_sync=1

""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""

