
# Training & Learning

## 培训主题计划
1. DevOps体系及在运维中的应用

1. 运维监控技术和体系构建

1. 云原生环境下安全治理
  - ppt材料已整理，需补充

1. 云宽带技术分享
  - ppt材料已整理

1. SDN技术漫谈
  - ppt材料整理中，学习中

1. 云原生技术及环境构建实践


## 云原生环境下安全运维分享

apiVersion: policy/v1beta1
kind: PodSecurityPolicy
metadata:
  name: example
spec:
  privileged: false  # Don't allow privileged pods!

apiVersion: policy/v1beta1  
kind: PodSecurityPolicy  
metadata:  
  name: read-only-fs  
spec:  
 readOnlyRootFilesystem: true


spec:  
 MustRunAsNonRoot: true

## 运维监控技术和体系构建


## SDN


## references
- [容器云应部署在裸机上还是虚拟化环境上](http://www.360doc.com/content/20/1225/10/29585900_953362411.shtml)
- [云原生笔记](https://skyao.io/learning-cloudnative/docs/introduction/history.html)
- [美团外卖持续交付的前世今生](https://www.sohu.com/a/376172482_198222)
- [一个白牌厂商视角：极简交换机NOS演进史](https://mp.weixin.qq.com/s?__biz=MzAxMDA1NjMwMQ==&mid=2651747258&idx=1&sn=0fbefa807c306e24e994ceebf32b05d3&chksm=80acf4b8b7db7daee70d93b7eb4afbab59400c5fab9d99ad67bf55771d381c583a103fe89e90&scene=21#wechat_redirect)
- [白盒交换机操作系统混战](https://www.sdnlab.com/21270.html)
- []()
- []()
- []()


