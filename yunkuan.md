## 云宽

- 用户统计
    - 新增，拆机
    - 按地区分布

- [vxlan](https://blog.csdn.net/armlinuxww/article/details/109109909)
- vtep,vxlan tunel endpoint,用户侧/边缘设备
- vni,vxlan network identifier,一个VNI代表了一个租户，属于不同VNI的虚拟机之间不能直接进行二层通信。VXLAN报文封装时，给VNI分配了24比特的长度空间，使其可以支持海量租户的隔离。

### 5大场景方案
- EDR+MEC, MEC安全能力方案
- 新城vxlan方案
- iTV软终端方案

