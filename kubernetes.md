
# Kubernetes

## questions
- chart, image 关系
- kubernetes ,docker 关系
- kubernetes主节点数量
- 应用部署在worker上？
- k8s安装，应用部署

Pod作为基本的执行单元，它可以拥有多个容器和存储数据卷

Kubelet才是Pod是否能够运行在特定Node上的最终裁决者，而不是scheduler或者DaemonSet。


## 使用
kubectl top node
kubectl top pods -A

kubectl describe nodes beta-ceshi-funeng-1

## helm
- 管理、编辑与更新大量的Kubernetes配置文件。
- 部署一个含有大量配置文件的复杂Kubernetes应用。
- 分享和复用Kubernetes配置和应用。
- 参数化配置模板支持多个环境。
- 管理应用的发布：回滚、diff和查看发布历史。
- 控制一个部署周期中的某一些环节。
发布后的测试验证。


## [业务连续性](https://support.huaweicloud.com/bestpractice-cce/cce_bestpractice_00223.html)

## Some Features
- 使用hostAliases配置Pod /etc/hosts

## deployment / release
- [Kubernetes蓝绿、金丝雀、滚动部署](deployment.md)


## 容器化改造
- [对传统应用进行容器化改造](dock_trans.md)
- Docker vs. Containerd

CCE支持用户选择containerd和docker作为运行时组件：
Containerd调用链更短，组件更少，更稳定，占用节点资源更少。 建议选择containerd。
当您遇到以下情况时，请选择docker作为运行时组件：
  - 如需使用docker in docker。
  - 如需在CCE节点使用docker build/push/save/load等命令。
  - 如需调用docker API。
  - 如需docker compose或docker swarm。

- [云容器引擎 CCE> 最佳实践> 微服务治理> SpringCloud微服务Isito迁移指导](https://support.huaweicloud.com/bestpractice-cce/istio_bestpractice_3012.html)
- [云容器引擎 CCE> 最佳实践> 云原生化改造迁移> 企业管理应用容器化改造（ERP）](https://support.huaweicloud.com/bestpractice-cce/cce_bestpractice_0001.html)

## [config yaml file](config_k8s.md)


## ingress
- [ingress & ingress control](https://www.cnblogs.com/linuxk/p/9706720.html)
- [ingress & ingress control](https://blog.csdn.net/weixin_42595012/article/details/100692084)
- [ingress 学习和实践](https://mp.weixin.qq.com/s/ZxyNCP32BQjQo5ygztJttQ)

## [kubectl](kubectl.md)

## rancher


## 日志采集
日志作为任一系统不可或缺的部分，在K8S的官方文档中也介绍了多种的日志采集形。
总结起来主要有下述3种：原生方式、DaemonSet方式和Sidecar方式。
- 原生方式：使用 kubectl logs直接在查看本地保留的日志，或者通过docker engine的log driver把日志重定向到文件、syslog、fluentd等系统中。
- DaemonSet方式：在K8S的每个node上部署日志agent，由agent采集所有容器的日志到服务端。
- Sidecar方式：一个POD中运行一个sidecar的日志agent容器，用于采集该POD主容器产生的日志。

优缺点：
- 原生方式相对功能太弱，一般不建议在生产系统中使用，否则问题调查、数据统计等工作很难完成；
- DaemonSet方式在每个节点只允许一个日志agent，相对资源占用要小很多，但扩展性、租户隔离性受限，比较适用于功能单一或业务不是很多的集群；
- Sidecar方式为每个POD单独部署日志agent，相对资源占用较多，但灵活性以及多租户隔离性较强，建议大型的K8S集群或作为PAAS平台为多个业务方服务的集群使用该方式。


## HA
master 挂了怎么办?
[搭建高可用的 Kubernetes Masters](https://kubernetes.io/zh/docs/tasks/administer-cluster/highly-available-master/)

## Some Questions
- OOMKilled

## 存储


## 有状态应用实践
- [分布式系统中的“无状态”和“有状态”详解](https://blog.csdn.net/universsky2015/article/details/105677992)
- 深入理解statefuleset － 有状态应用实践, 《深入剖析kubernetes》p133
- [在Kubernetes上运行有状态应用：从StatefulSet到Operator](https://www.cnblogs.com/sammyliu/p/11976202.html)

## References
- [demo](https://github.com/ewolff)
- [基于Kubernetes微服务Istio案例源码](https://www.jdon.com/51498)
- [Istio是啥？一文带你彻底了解！](https://www.sohu.com/a/270131876_463994)
- [k8s的持久化存储PV&&PVC](https://www.cnblogs.com/benjamin77/p/9944268.html)
- [K8S容器编排之Headless浅谈](https://www.jianshu.com/p/a6d8b28c88a2)
- [确定k8s的Annotation与Labels你用对了](https://cloud.tencent.com/developer/article/1833485)


