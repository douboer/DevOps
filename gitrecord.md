
## git使用笔记

#### 建立流程
1. 在github.com里，new repository创建远程仓库
2. 本地新建文件夹/已存在项目文件夹下
```
git init
```
3. 关联远程仓库
```
git remote add origin https://github.com/douboer/gt.git
```
4. 将本地仓库推送到远程仓库
```
git push -u origin master 
or:
git push origin master
```
或者从远程拉资源到本地
```
git pull origin master --allow-unrelated-histories
```

```
git init
git add .

git config --global core.excludesfile .gitignore
git config --global user.email "douboer@gmail.com"
git config --global user.name "douboer"

git rm -r --cached .
git add .
```

#### 修改.gitignore文件 不生效
原因是.gitignore只能忽略那些原来没有被track的文件，如果某些文件已经被纳入了版本管理中，则修改.gitignore是无效的。那么解决方法就是先把本地缓存删除（改变成未track状态），然后再提交：
```
git rm -r --cached .
git add .
git commit -m 'update .gitignore'
```

```
> ssh-keygen -t rsa -C "douboer@gmail.com"
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\admin/.ssh/id_rsa): yes
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in yes.
Your public key has been saved in yes.pub
```


```
// modify commend
git commit --amend

//fatal: remote origin already exists.
git remote rm origin

git remote add origin git@github.com:douboer/gt.git
```

```
…or create a new repository on the command line
echo "# test" >> README.md
git init
git add README.md
git commit -m "first commit"
git remote add origin https://github.com/douboer/gt.git
git push -u origin master
```

```
…or push an existing repository from the command line
git remote add origin https://github.com/douboer/gt.git
git push -u origin master

git pull origin master --allow-unrelated-histories
```

```
git remote add origin https://github.com/douboer/gt.git

origin相当于远程仓库的一个别名
可以通过删除
git remote remove name   

查看该 origin，该命令将列出远程仓库的 URL
git remote -v
```


#### 修改 .gitignore 文件 立即生效
有时候需要突然修改 .gitignore 文件，随后要立即生效
```
git rm -r --cached .  #清除缓存  
git add . #重新trace file  
git commit -m "update .gitignore" #提交和注释  
git push origin master #可选，如果需要同步到remote上的话  
```


#### 查看远程分支
```
git branch -a
```
#### 查看本地分支
```
git branch
```
#### 创建分支
```
git branch test
```
#### 把本地分支推到远程分支 
```
git push origin test
```
#### 切换分支到test
```
git checkout <name> 命令可以切换到现有的分支。可以使用 git checkout -b <name>命令创建一个新的分支并立即切换到它。
```
#### 删除分支
```
git branch -r -d origin/branch-name
```
#### 将删除的分支推送到远程，并在远程删除这个分支
```
git push origin :branch-name  
```

#### 添加、提交和推送更改到你的远程仓库
```
git remote -v
git remote add origin <url>
git remote set-url origin <url>
```


#### 几乎每个开发人员都在使用 Git，当然很可能是 GitHub。但大多数开发者大概有 99％ 的时间只是使用这三个命令：
[-](https://linux.cn/article-8841-1.html)
```
git add --all   = git add . + git add -u
git add -A      = git add . + git add -u
git add .
git commit -m "<message>"
git push origin master
```

:warning:
```
> git push origin master
::error: src refspec master does not match any.
error: failed to push some refs to 'git@github.com:douboer/wisim.git'
```
==SOLVE:
```
git commit -m "wisim"
```

:warning:
```
> git add .
warning: LF will be replaced by CRLF in .gitattributes.
The file will have its original line endings in your working directory
warning: LF will be replaced by CRLF in WiSim.cpp.
```
==SOLVE:
```
git config --global core.autocrlf false
```

- 创建dev分支
```
git branch dev
```
- 切换到dev分支
```
git checkout dev
```
- 查看是否切换到dev
```
git branch -a
git branch
```
- 本地push到dev分支
```
git push origin dev
```
- dev分支强制覆盖master分支
```
git push origin dev:master -f
```

:warning:
```
> git remote add origin https://github.com/douboer/wisim.git
fatal: remote origin already exists.
```
==SOLVE:
```
git remote rm origin
git remote add origin https://github.com/douboer/wisim.git
```

:warning:
```
> git push origin master
To https://github.com/douboer/wisim.git
 ! [rejected]        master -> master (non-fast-forward)
```
==SOLVE:
```
#### 强制覆盖：
> git push -f
fatal: The current branch master has no upstream branch.
To push the current branch and set the remote as upstream, use
    git push --set-upstream origin master
>  git push --set-upstream origin master
To https://github.com/douboer/wisim.git
 ! [rejected]        master -> master (non-fast-forward)
> git push -f --set-upstream origin master
Total 0 (delta 0), reused 0 (delta 0)
```

:warning:
```
> git push origin master
git: 'credential-cache' is not a git command. See 'git --help'.
Everything up-to-date
```
==SOLVE:
```
> git config --global credential.helper wincred
```

```
$ git push origin
```
*上面命令表示，将当前分支推送到origin主机的对应分支。
如果当前分支只有一个追踪分支，那么主机名都可以省略。*
```
$ git push
```
*如果当前分支与多个主机存在追踪关系，那么这个时候-u选项会指定一个默认主机，这样后面就可以不加任何参数使用git push。*
```
$ git push -u origin master
```
*上面命令将本地的master分支推送到origin主机，同时指定origin为默认主机，后面就可以不加任何参数使用git push了。
不带任何参数的git push，默认只推送当前分支，这叫做simple方式。此外，还有一种matching方式，会推送所有有对应的远程分支的本地分支。Git 2.0版本之前，默认采用matching方法，现在改为默认采用simple方式。*


#### 本地已有新项目gplot上传个github
```
1. login github, push "new repository" to Create a new repository
   input repository name then save setting
2. locate project directory
   $ git init
   $ git add .
   $ git commit -m "gplot"
   $ git remote add origin https://github.com/douboer/gplot.git
   $ git push -u origin master
3. 日常修改上传
   $ git add .
   $ git commit -m "gplot"
   $ git push
```

#### 冲突解决
1. 方案一，强制覆盖本地代码
把你修改的代码进行备份，然后执行命令：
git reset --hard origin/master
git pull
从你备份好的文件当中把你写的代码拿过去，修改完成再进行git push 


#### 超过100M文件github上传失败问题
git lfs install
git lfs track xxx # xxx是大于100M的文件


