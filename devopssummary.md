
# devops master summary

- 是什么？文化？
- devops实践？
- devops工具？
- 价值流具体化、可视化？
- 需求拆分技术？
- BA QA/Tester Dev - BDD技术？
- 谁决定能不能上线？


# 要点
1. DevOps Mindset? **非谴责**, 高效, 亲和
1. Devops文化转变包含? 亲和, 协作, 信任, 同理心, 不谴责, **单件流**  **Trust each Other，Blameless**
1. DevOps方法论: PDCA，KAIZEN，可视化控制&管理（obeya作战室：共享信息, 快速决策）
1. 聚焦软件交付的业务价值（持续、顺畅、高质量交付有效价值）
1. 创建一个**拉式部署流水线**
1. 质量内建 + 面向上游（从Ops到Dev） + **人人对质量负责**
1. 部署脚本：使用同样的部署脚本在不同环境中部署，确保部署流程幂等性
1. 尽早发现问题(编译错误，测试失败，环境问题让提交失败) **fail fast fail cheap**
1. 任何情况下，都不能破坏流程。紧急修复版本也要走 构建/部署/测试/发布的流程，与变更没区别
1. 发布：蓝绿发布，金丝雀发布, AB测试
1. 紧急修复 or 回滚
1. 基础设施重建：Auto provisioning - auto maintainance
1. 创建**类生产环境**，环境配置变更应能够触发流水线
   确保交付团队能得到应用程序在类生产环境上的不断反馈
1. 创建监控策略：collect, store, dashboard, log, monitor
1. 企业治理：
    - 符合度(conformance)，合规性，关注遵从性，保障，监管，责任，透明管理
    - 执行度(performance)，绩效，关注业务和价值
1. 验收测试的最佳路径**happy path**
1. 交付过程中，缺陷被发现得越早，修复它的成本就越低 fail fast
1. **拉灯**

紧急变更不应该成为不走变更流程的理由

#### 精益8大浪费 DOWNTIME

| type | description | 解决 |
| ---- | ---- | ---- |
| defect | 缺陷 | poka-yake(防错设计) |
| overproduction | 过度生产 | 可视化，拉动生产 |
| waiting | 等待 | 识别约束，减少等待 |
| non-utilized people | 未充分利用人员 | kaizen，学习型组织 |
| transportation | 搬运 | 单件流 |
| inventory | 库存 | 可视化，拉动生产 |
| motion | 动作 |  |
| extra processing/feature | 镀金 | 5why |

#### 需求/用户故事原则 INVEST

type | x
-- | --
独立的（Independent） | 解耦
便于沟通（Negotiable） | 减少与客户等相关方的沟通成本
有价值的（Valuable） | 对客户有价值
可估计的（Estimable） | 开发者便于估计工作量
小的（Small） | 短小有代表性，敏捷迭代
可测试的（Testable） | 用户故事已完成的标准，或者说能够确认已完成


#### 个人实践

关于测试
1. 类生产环境作为beta测试环境，可以是本地资源
2. 生产中把部署和发布解耦，发布采用金丝雀或蓝绿，蓝绿对资源要求太高，采用金丝雀发布，测试/支撑/业务人员对金丝雀做小批量验证，只验证主流程，做冒烟。用Prometheus做pod监控。
3. 验证有问题，删除新版本Pod；验证通过，新版本Pod扩展到老版本资源。


```
约束点在哪里？ --> 如何改善约束点？ --> 突破约束点 -->
  ^                                                  |
  |__________________________________________________|

可能约束点
1. 环境搭建
2. 代码部署
3. 测试的准备和执行

监视缺陷和镀金

```


开发运维“三步工作法”，它旨在阐明指导开发运维的流程与实践的价值观与理念。

### 第一工作法
是关于从开发到IT运维再到客户的整个自左向右的工作流。**为了使流量最大化，我们需要小的批量规模和工作间隔，绝不让缺陷流向下游工作中心**，并且不断为了整体目标（相对于开发功能完成率、测试发现/修复比率或运维有效性指标等局部目标）进行优化。必要的做法包括持续构建、集成以及部署，按需创建环境，严控半成品，以及构建起能够顺利变更的安全系统和组织。

### 第二工作法
是关于价值流各阶段自右向左的快速持续反馈流，放大其效益以确保防止问题再次发生，或者更快地发现和修复问题。这样，我们就能在所需之处获取或嵌入知识，从源头上保证质量。

“必要的做法包括：在部署管道中的构建和测试失败时“停止生产线”；
日复一日地持续改进日常工作；创建快速的自动化测试套装软件，以确保代码总是处于可部署的状态；
在开发和IT运维之间建立共同的目标和共同解决问题的机制；
建立普遍的产品遥测技术，让每个人都能知道，代码和环境是否在按照设定的运行，以及是否达到了客户的目标。

### 第三工作法
是关于创造公司文化，该文化可带动两种风气的形成：不断尝试，这需要承担风险并从成功和失败中吸取经验教训；理解重复和练习是熟练掌握的前提。”

“尝试和承担风险让我们能够不懈地改进工作系统，这经常要求我们去做一些与几十年来的做法大不相同的事。一旦出了问题，不断重复的日常操练赋予我们的技能和经验，令我们可以撤回至安全区域并恢复正常运作。

必要的做法包括营造一种勇于创新、敢于冒险（相对于畏惧或盲目服从命令）以及高信任度（相对于低信任度和命令控制）的文化，把至少20%的开发和IT运维周期划拨给非功能性需求，并且不断鼓励进行改进。



### references
- [使用Kubernetes演示金丝雀发布](https://www.cnblogs.com/rexcheny/p/10740536.html)
- [istio完成金丝雀、灰度发布](https://blog.csdn.net/qq_42150559/article/details/96136245)
- [使用原生k8s及helm完成灰度(金丝雀)发布](https://blog.csdn.net/qq_42150559/article/details/97143825)
- [Kubernetes的部署策略，你常用哪种？](https://www.sohu.com/a/318731931_100159565)


- [写给新人的BA工作说明书](https://www.jianshu.com/p/9efbf1233a7e)
- [DevOps实施：从敏捷文化与配置文件的困惑说起](http://www.suphp.cn/yunweipai/35/23835.html)
