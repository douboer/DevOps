
## 老孟的故事

老孟姓名孟凡克，原名Michael Mandell，地道美国人。老孟在中国置房娶妻、并打算在中国育儿。

老孟应该是2002年（也许是2003年）离开美国波音公司chief Scientist岗位，再之前在美国军工企业Hughes Space and Communications公司也是Chif Scientist。老孟自己说“……背井离乡远涉重洋到了这片陌生的土地，无法预知的语言的障碍和生活的困难接踵而来。古人云：独在异乡为异客……”

当时进公司老孟是第一个面试的，2004年，记得老孟问了些诸如正弦曲线、数值算法之类的数学问题，这些记忆都成一锅稀饭了，印象深刻唯有老孟的中文，作为第一次来中国差不多半年的美国人，这样的中文程度可谓流利了。所以，关于老孟所说的“无法预知的语言的障碍”，极少有人会相信。进公司久了，和老孟接触多了，就发现日常有两爱好:
- 一是，老孟在与人聊天、尤其人多的场合无聊之时，喜欢用平仄不分的音调援引简单的中国古诗，摆弄汉字四字成语，这总能博新朋友刮目，我们则习以为常，偶会帮老孟纠正下发音;
- 二是，工作之闲老孟喜欢拿一支黑色的滑动铅笔往一方小废纸片上涂画些符号，凑近一看，方知这些符号大部分是些类蝌蚪文的汉字（老孟的英文字写的还是颇具美感的，看来是没得汉字书法之精要），于是，结论是，老孟说的比写的好多了。

据说，较刚来公司时老孟一年后中文水平精进，这从老孟在饭桌上时有用中文插科打诨，从老孟后来的一些中文邮件上可得印证。
老孟的日常爱好和后来的中文精进，推想老孟刚来中国时中文也许确实还比较烂，也许那时老孟确实有“无法预知的语言障碍”的感觉。

老孟的本地化进程不止体现在语言上。老孟到中国后易居多处，也许这当中也包含老孟自己说的“生活的困难接踵而来”的异乡异客的辛酸吧。 然而，老孟还是能够在易居过程的某一个短暂的停靠点上找到一些乐趣。某段时间老孟在某老小区居住，据说该小区的大爷、大妈和老孟都挺熟的，碰到老孟都会打声招呼，偶尔也寒暄几句。老孟与邻居时有些小故事传播。听到这些故事，总想到自己这幢楼，两对门邻居互不招呼。不知是我们的进步，还是老孟的退化。

老孟某段时间搬迁新居，恰逢公司搬迁，住处与上班处距离20里有余，附近同事多搭公车或自己开车上班，老孟偏选择骑单车上班。这新的单车本就不咋的，半年折腾下来，就成除了车铃不响每个部位哗啦响、放在门外不上锁也安全的破玩意，而老孟依然执着的乐此不疲的融入本地单车上班族，骑二十来里路大汗淋漓的来上班。可，今天，工作压力大了，体力的付出似乎俞显奢侈，大家更愿津津乐道自行车升级为电瓶车再升级为汽车。

老孟其实不洋，喜腰间别一手机并加套，可这，在这里流行过，又不流行了，不知老孟是因为流行还行因为方便。可这些在我看来，倒显老孟的帅真和亲和。

老孟现有些发福，尤其以肚子为甚，所以，基本上老孟说不上帅。我看到的老孟的装束都是“T恤+牛仔”或“T恤+牛仔+外套”，偶辅之衬衫。 据某女同事说，老孟刚来中国时，某次参加公司年会，一身黑色西服，这个女同事描述当时的感想是：这个老外真帅。可见，老孟很帅过。 老孟的发福，尤其是肚子变样，其责任完全的归于老孟的随和和同事的热情。老孟随和，所以，颇具人缘，老孟喜欢泡吧，所以常有同事邀其同饮，日积月累修成啤酒肚。

与老孟一起工作始于2004年，参与老孟主导开发的一个项目，到2006年老孟调去深圳做WiMAX芯片开发。期间，老孟的建议和指点虽寥寥数语却总能起到指点迷津披荆斩棘现坦途的作用。

老孟头衔很多，孟老师、孟博士、专家、首席科学家……，这些头衔对于老孟，都是货真价实无虚假成分的。在我看来，老孟本质上是一个程序员，老孟具有一个好程序员的特质，老孟有扎实的数学基础，严谨的思维，对未知东西热烈的好奇，热情的付出，执着的追求。观老孟写的东西。代码简洁清晰而富逻辑、具美感，折射出其深厚的编码素养。当然老孟写代码似乎有一个坏习惯--代码注释极少，有注释处也是极其精炼，因此，对于阅读者是一个不小挑战，极不符合开源精神，当然，这对于老孟自己并不是问题，一些"年代久远"的代码在老孟眼里依然脉络清晰，能很快抓住主旨要害。老孟写的文档或论文看过的不多，映像深刻的有两篇，其一是关于一个网络仿真工具的数据结构和其中算法的介绍；其二是关于无线传播模型及其算法的介绍。老孟的文档侧重实用性和可实现性，少有花里胡哨的东西，老孟的文档是其对于实现的总结，当然实用和可实现。老孟对于算法的介绍也是逻辑清晰，由浅入深，平实朴素，而其中又不乏独具匠心的巧妙设计。当然，写学术性论文一个很重要的技术就是必须要安排一段搞一把公式把读者击晕，这样就差不多成功了。

老孟性情温和、或者说随和、或者说亲和更贴近些。在中国几年，老孟已深谙中庸之道，不知是品性使然，还是被国人中庸感染。
其实，现在和他一起工作的很多中国人都不那么中庸了。

- 2008-05-29
