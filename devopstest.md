# devops测试

## 一、选择题（20）

1. 验收测试的最佳选择?
a. Happy Path
b. 回归测试
c. alternate Path
d. Sad Path

a. happy path 相当于做主流程

2. 云计算的哪个特性能实现自动部署? 
a.  标准栈
b.  规模化
c.  虚拟化
d.  通用性

a. 标准栈使得自动部署成为可能

3. 软件交付过程中，让整个过程自动化，并能够及时发现问题和修复问题，在此过程中,以下实践不是反模式选项是：
a，要求一份详尽文档，该文档描述了每个步骤中容易出错的地方
b，开发完成后才向类生产环境部署
c，提前频繁的做让你感到痛苦的事
d，手工进行生产环境的配置

c. 痛苦的事情频繁做

4. 良好的配置管理过程，是实现开发和部署过程可控和可重复的基础，以下哪个内容不是良好配置管理策略：
a. 能够再现所需的任何环境。
b. 为了达到系统运行稳定的目标，固化配置，任何修改都做登记。
c. 能够追踪某个具体环境的某次修改，并能够追溯修改源，以及什么时间谁做了修改。
d. 增量式修改，并可将修改部署到任意一种环境中。

b. 不是敏捷策略。影响迭代。

5. 每个应用程序都依赖于软件、硬件、基础设施等才可以正常工作，这些内容称作应用程序的环境，环境管理与配置管理同样重要。以下哪种不是良好的环境管理实践：
a. 环境管理的关键是通过一个自动化过程来创建环境。
b. 因为环境管理的重要性，一旦系统出问题，派遣资深专家花费不确定的时间来找到问题，并修复它。
c. 修复某个环境可能需要大量时间，因此，最好在可预见的时间里重建环境，并将其恢复到某个已知的正常状态。
d. 创建一个类生产环境，配置问题可在早期测试中发现。

b. 1. 自动化、简单化、可量产、可重现，不是依赖资深专家

6.  公司打算构建一条部署流水线，公司领导希望实现频繁发布。
有团队成员认为：这条部署流水线最重要的是自动化。团队首先要让它自动化起来。
这种说法对吗？
a.  是的，这是正确的。部署流水线自动化是提升效率的最重要因素。
b.  是的，这是正确的。关注自动化部署流水线的创建，克服之后可能遇到的潜在问题。
c.  不，这是错误的。完成单件流及一个可靠的部署流程是优先级最高的任务。该流程的自动化可以暂缓实施。
d.  不，这是错误的。首先应当自动化的是测试流程而非部署流水线。

c, 无论何时，所有部署流水线首先应当是单件流程的部署流水线，无需自动化就可高效运行。一旦该流水线稳定确立，就有机会选择可行的流程实施自动化。但是，构建稳定的部署流水线永远比自动化更重要。

7. 有很多方法可以使组织趋向成熟，下列哪种方法不会使你的devOps组织更趋成熟？
a. 明确定义目标定和里程碑，帮助团队成员判断其日常活动是否有价值。
b. 明确定义流程，支持并促使团队成员逐日改进流程。
c. 对会议的所有内容进行记录，使你的团队成员可以很方便的了解到每次沟通的内容信息。
d. 监控并记录每天的活动，以找出小范围内每天取得的进步并予以赞扬。

c, 这无助于devOps组织的成熟。是否要对会议进行全程记录并再次审查，并没有严格的要求。有必要记录达成共识的内容，而不是记录整 场会议。（文献：持续交付，第十五章）

8.  你认为自己的开发团队是一支真正的团队。
你觉得有什么确切的特征表明这是一支团队而不只是一个小组呢？
a.	该团队遵守在团队会议中共同制定的规则。
b.	团队召开自己主持的高效会议。
c.	团队以稳定的工作节奏，朝着共同的目标推进。
d.	该团队通过质询负责某项工作的团队成员的方式来解决问题。	

d 一支真正的团队能够维持稳定的工作节奏，并能够始终向着共同的目标努力。（文献：Effective devOps，第九章）

9.
为采用整体方法处理所有基础设施，应当遵循哪两条原则?
a.
1.	你的基础设施应具备的状态需要通过变更控制配置来确定。
2.	你应当通过监控与事件管理，及时了解基础设施的准确状态。
b.
1.	需要通过变更控制配置来确定你的基础设施应具备的状态。
2.	你应当通过仪器仪表及事件管理，始终了解基础设施的确切状态。
c.
1.	你的基础设施应具备的状态需要通过版本控制配置来确定。
2.	你应当通过当前事件与事件管理，始终了解基础设施的确切状态。
d.
1.	你的基础设施应具备的状态需要通过版本控制配置来确定。
2.	你应当通过仪表盘与监控始终了解基础设施的确切状态。

d, 为采用整体方法处理所有基础设施，应当遵循这两条核心原则。（文献：持续交付， 第十一章）

10.
在敏捷和devops中吸收了很多精益核心概念。实施devOps时，将精益的相关概念应用在devOps过程中，有助于成功实施devops。
在此过程中，精益管理的哪些原则或实践方法最有效?

a) Kaizen(专有词，意为小的、不花钱的持续改善)
b) 5S - 整理（SHIRI）、整顿（SEITON）、清扫（SEISO）、清洁（SEIKETSU）、素养（SHITSUKE）
c) Obeya系统(可视化管理)
d) 单件流与质量检查

d,
创建一个可行的、单件部署流水线将有 助于成功实施devOps。devOps中最重要的工作在于构建从开发部门到运维部门的上游流程，尤其是针对单一部署流水线。质量检查（JKK）是能够达成这一目标的最有效的工作行为。（文献：《企业devOps的成功之路》）


11.  持续集成不会单独的帮你修复构建过程，在中后期开展持续集成，意味着在构建过程需要耗费大量工作。
为了使持续集成更高效，早期阶段，一般不会涉及的工作是？
a. 频繁提交代码到版本控制库
b. 创建分支，并尽早提交代码到分支
c. 创建自动化测试套件
d. 保持较短的构建和测试过程

b. 应提交到主干，分支容易破坏即时集成
持续交付 P46

12.  关于版本控制，以下说法不正确的是？
a. 源代码必须纳入版本控制。
b. 配置信息必须纳入版本控制。
c. 为了加快发布周期和提高软件质量，将编译器或其他相关工具的二进制镜像纳入版本控制。
d. 为了加速打包，将源代码编译后的二进制文件纳入版本控制。

d. 不推荐，1. 比较大，2. 每次重新构建都会重新生成二进制文件，无必要


13. 一个开发团队对devOps感兴趣。他们主要感兴趣的对象是持续集成（cI）。他们目前开发并维护着三种主要解决方案及四种次要解决方案。他们采用Scrum实践方法。每次冲刺都需要四周时间，平均每十天到十五天对测试环境都有一次提交发布，平均每个月对生产环境有一次发布。他们想为管理层制定一个定性的商业论证，来支持他们为创建持续集成实践模式而付出的投资与努力。
持续集成有哪些显性收益对该商业论证最为有利？
a. 每天对测试环境进行一次部署能极大的提高商业效益并且大大缩减开发成本。
b. 这有助于提升团队精神。由于公司已经在使用Scrum，持续集成将为公司业务带来显著的益处。
c. 它通过更好的集成测试提高了业务稳定性，同时维持发布速度以防止产生额外成本。
d. 在生产环境中，每天进行一次信息发布能够提升业务收益，并大大减少开发成本。

d, 更快速的向生产环境发布是持续集成的一大主要益处，此外还包括更快速地发现故障以减少开发成本和故障修复成本。（文献：《持续交付》， 第三章——持续集成）

14.  关于使用脚本去构建和部署流程自动化，以下说法正确的是？
a. 每种环境采用一个脚本，并将其作为版本控制系统的一部分加以维护。
b. 不同环境使用不同的特定脚本，以解决环境之间的差异问题。
c. 不同环境采用同样的脚本，对特定的配置采用手动参数。
d. 采用同一脚本在不同环境中进行部署，并单独管理配置信息。

d,
脚本应当保持一致，才能保证构建和交付流程得到有效测试。环境之间（如URI和IP等）的差异应当作为配置管理流程的一部分予以处理。（文献：《持续交付》，第六章——构建 与部署脚本处理的原则与方法）


15. 当有运维侧变更时，运维部门告知开发部门的最佳时间是何时？
a. 无需告知开发团队。运维侧的变更仅运维团队知晓即可。
b. 立刻执行。应当尽快通知开发部门。
c. 次日早晨的Scrum会议中。
d. 当运维团队已经完成验收测试时。

b, 应当立即告知开发部门，让他们能够预见潜在的风险和问题。（文献：企业级devOps的成功之路）

16. 考虑对基本部署流水线进行具体解析。
哪个阶段表明该系统在功能性与非功能性层面均发挥作用？
a. 自动化验收测试
b. 构建与单元测试
c. 手动验收测试
d. 版本控制

a. 自动化验收测试阶段表明，系统在功能性与非功能性层面上工作正常，在行为上它能满足用户的需求和客户的规格要求。（文献： 《持续交付》，第八章）


17. 开发团队当前在测试中遇到诸多挑战。目前他们使用人工验收测试流程。
开发者认为他们所创建的单元测试是十分周密的，可以避免回退。
在每次发布时，开发团队都需要花费100万在人工验收测试环节。
领导层要求开发团队实施自动化验收测试，以降低测试的总成本并尽可能减少引入生产环境中的代码缺陷数量和回退次数。
在依照自动化需求确定应用程序的验收标准时，应当遵循什么原则？
a. agile（敏捷）原则
b. aTaM（架构权衡分析法）原则
c. dIVEST原则
d. INVEST原则

d, 验收测试源自验收标准，因此你的应用程 序的验收标准的制定应当考虑自动化因素，遵循INVEST原则；
Independent (独立性)、Negotiable (可协商)、Valuable(有价值)、Estimatable(可估计)、Small(小而少)、Testable(可测试)（文献：持续交付，第八章——如何创建可维持的验收测试套件）


18. 自动化测试是高效软件发布和实现持续交付的基础，以下那一类测试不宜全面自动化。
a. 容量测试
b. GUI测试
c. 单元测试
d. 探索性测试

d. 探索性测试一般需结合人工测试

19.
公司正在使用devOps。该公司实施了持续部署，并具备高度自动化验收测试和每日向生产部交付新软件的稳定部署流水线。公司有一个巨大的数据库及众多用户。该公司具备全面可靠的容量测试策略。由于该公司环境广大而复杂，随着每个新版本的发布，生产部就会出现一些小故障。
什么策略能够最有效地帮助该公司预防这些故障？
a) 采用金丝雀发布
b) 自动化容量测试
c) 降低交付率
d) 采用蓝绿部署

a. 金丝雀发布包括向生产服务商中的一小部分推出新版本的应用程序，以快速收集反馈。这能够快速发现新版本中出现的所有问题，而不会对大多数用户产生影响，因为工作量是逐渐增长的；同时这一做法还能确定响应时间及其他工作表现衡量标准，减少新版本发布的风险，并帮助尽快发现与修复漏洞。（文 献：持续交付，第十章-金丝雀发布）

蓝绿部署需要大量资源，因而在该情境 中代价高昂。此外，若需要回退，在大型数据 库中采用这一策略可能导致故障或只读情况发 生。另外，这也无助于容量测试效率的提高。

20.  公司正在尝试转变，并开始使用devOps的方式开展工作。你的团队也在经历这一转变。
你正在参与讨论代码提交阶段的最佳实践。
某同事说：“当某一构建遭到破坏且无人担责时，我们应当找出造成破坏的人并要求他们展开工作，以保证他们能修复这一构建。”
这样做合适吗？
a. 是的。只有破坏构建的人才能够修复它，因此你应当找到负责人，即使这样可能会让人感觉不舒
服。
b. 是的。你应当始终为破坏构建负责。如果你不负责，你的同事将可能强制执行这项规定。
c. 不，devOps环境中不存在追责。若同事不承担责任，不要强迫他们。
d. 不，你应当首先修复构建。然后抽出时间，确定相关负责人并进行处罚。

c, Devops文化不提倡强迫任何人做任何事。犯错是可以接受的。团队成员共同协作以克服各种错误或挑战。（文献：持续交付，第三章；高效的 devOps，第一部分）


## 二、简答题（5）
1. 简述devops能带来哪些效果？
    快速发布
    降低成本
    持续反馈，提高软件质量
    更关注业务产出，给客户带来价值

2. 每个系统都会遇到这种情况：发现一个严重缺陷，必须尽快修复。简述紧急变更过程?
    需牢记：任何情况下，都不能破坏流程，紧急修复版本同样走构建、部署、测试、发布流程，与其他代码变更没有区别。
      申请变更(Request for change,RFc)
      分析影响
      通知利益相关方

3. devops文化转变包含哪些?
    高效
    亲和
    协作
    高度信任
    同理心
    非谴责
    单件流

4. 持续集成不光是一些工具组合，更是一种实践，它的有效性也依赖于团队纪律。简述cI(持续集成)的好处，及为了实现持续集成目标，有哪些良好的实践?
    好处：
      软件在任何时候可工作
      更少的bug
      更低的成本
      更快发现bug
    良好实践或纪律：
      构建失败后不要向版本库提交新代码, 确保软件一直可工作
      本地构建并运行提交测试，测试通过才继续工作
      不要将失败的构建注释掉
      为自己导致的问题负责
      所有人为质量负责
      ...
     
 5.  在自动化运维中，要实现基础设施和应用程序的监控策略，通常要考虑哪些方面内容或从哪些方面着手？
      采集数据，软硬件运行情况，健康度，统计信息等, 通过SNMP/TR069等
      记录日志，解析、分析、统计日志文件，并关注日志等级
      建立信息展示板(dashborad),可视化
      建立自动通知/告警机制

## 三、论述题（2）
1. 在软件发布过程共，我们常常看到测试、运维、开发团队协作不畅导致的流程受阻，部署流水线从端到端来贯穿流程，实现自动化构建、部署、测试和发布。公司推进敏捷/devops，请结合实践，论述如何构建一套部署流水线，以及这套部署流水线如何在实践中更好的实现持续交付。

内容要点：
结合实践来阐述
     1. 部署流水线流程
          提交,版本控制库
          自动构建和单元测试（可能涉及自动配置管理）
          部署二进制包
          自动化验收测试(一般做冒烟测试，也可能包括自动化容量、安全等非功能性测试)

          运维一键部署到生产环境（容器化,生产环境配置，部署二进制包）

          流程的起点是开发人员向版本控制库提交代码，流水线对提交作出响应，触发流水线的一个实例，编译代码，运行单元测试，执行代码分析，创建软件的二进制包；如果所有单元测试通过，代码符合标准，完成软件打包；之后触发自动化验收。
       2. 构建流水线过程
          对价值流建模并创建简单的可工作框架（比如流行的jenkins）
          将构建和部署流程自动化
          将单元测试和代码分析自动化
          将功能（验收）测试自动化
          将发布自动化
       3. 一些良好实践
          部署流水线的成功的关键之一是各个环节能够尽快得到反馈
          项目较大情况下，全量或回归测试可能耗费大量的时间，可能打断持续集成效率，在流水线中往往只做冒烟测试，通常要在一两个小时内完成该过程。
          确保生产中部署的包与流水线生成的二进制包是同一个，或者对包做HaSH验证。重新创建二进制包将带来不一致风险。
          如果某个环节失败，应停止整个流水线。整个团队对失败负责。
          象其他交付的应用一样，对部署流水线进行增量式实现，不断改善和重构。
          让每个环节可视化，让大家能够清晰的看到哪个环节有什么问题。
          

参考<持续交付>第五章相关内容

2. 
一个电信级应用部署在云上，随着业务不断演进，系统用户和数据量不断增加，云资源的稳定性和系统的复杂性带来的系统服务中断的风险越来越大，系统中断的影响也随之增加。请结合实践，论述从哪些方面来提升系统可用性或提升系统容灾能力。

从以下要点结合实践展开论述：

架构手段：从以下角度结合应用进行分析：
        - 冷、热备份，心跳检测保活
              如通过LVS、haproxy、keepalive等方式
        - 集群 & 负载均衡 & 分布式
              负载均衡架构是多个服务做一堆同类的事情，每件事情相互独立；
              分布式的架构则是将一件复杂的事情分解出多个部份，由多个服务去做，再通过架构实现多个分解事情处理结果的合并。
        - 多中心，异地异机房，涉及负载分担和数据同步等问题的考虑
        - 纵向扩容，如加服务器资源，换性能更强的服务器提升服务能力
        - 横向扩容，对业务、数据库、区域等进行划分
        - 业务服务的拆分
        - 数据库的读写分离
        ...

运维手段：
        - 虚拟化、容器化
        - 监控手段，发现机制和加速处理机制等
        - 演练和一些馄饨学工具故障模拟
        - 自动化运维，监控预警等手段
        - 应急预案
        ...



2. 创建可执行文件的工具是一种什么样的构建工具? Make, 面向产品
3. 一出问题就骂外包: 加强协作, 不指责.
4. 告诉老板devops能带来哪些效果: 快速发布, 降低成本, 直接支持业务产出
5. devOps开始时要做什么? 愿景, 目标.
6. devops团队的成员? 6-8人, cross function 跨职能
7. 紧急变更打补丁? RFc 申请变更, 分析影响, 通知利益相关方
8. 云计算的哪个特性能实现自动部署? 标准栈
9. 云的哪个特性不让人放心想上云? 安全, 服务级别(SLa)
10. 哪个不是实现良好配置管理的策略?不能把二进制放到配置管理中
11. 什么是管理基础设施的原则? 原则是后面个自动化, 版本控制, 监控
12. 什么是构建依赖, 什么是运行依赖? 编译是构建依赖
13. 手动软件发布过程的优点? 加强协作, 加强Review
14. 什么不是部署管道的一部分?
15. 一个新版本每三个月发布一次是devops实践? 迭代中要进行增量发布
16. 为什么SLa对客户如此重要?  对客户有价值
17. 发生故障是基础设施重建? 自动化提供自动化运维
18. SOR? 协作型
19. 向其他同事抱怨不直接发生冲突? 避免/回避
20. devOps Mindset?  非谴责, 高效, 亲和
21. 什么是协作? 多人根据输入达成共同的输出
22. 什么是亲和? 不同群体
23. 如何使devops更加成熟? 戴明环, KaIZEN, 可视化控制和可视化管理.
24. devops文化转变包含什么? 新和, 协和, 高度信任, 同理心, 不谴责, 单件流
25. 项目在devOps中取得成功必须改变什么? Mindset,  千万不要选工具
26. 重复可靠是指什么过程? Release
27. devops反模式? 手工发布, 开发完后才部署
28. 迭代时间和频次取决于什么? 业务需求
29. 持续部署的优点? 性能和一致性不相冲突
30. Obeya作战系统? 共享信息, 快速决策
31. cI的好处? 更少的bug, 更低的成本, 更快发现bug
32. 既能保证质量又能提高发布效率?单件流
33. 构建失败时应该怎么解决? 谁提交失败谁负责修复o

---
devOps白皮书
第1部分:a journey to devOps The devOps framework should support business outcomes directly。 

第2部分:What is devOps for the enterprise system? devOps can also enable maturity by using W.E. deming’s Plan do-check-act cycle.

第4部分devOps body of Knowledge:第四节TPS (Lean) concept as foundation :building a stream-lined supply chain of IT services is difficult because there are many  items and it is necessary to change your mindset from the familiar existing development cycle and its methodologies.(JIT means building up a stream-lined supply chain with one-piece 
flow.) 

第4部分 devOps body of Knowledge:IT service management Service on just providing quick and frequent IT services and reliable operation and is led by the service  master. It is most suited for SoE and SoR continuity is an essential part of the warranty (fitness for purpose) of a service. If service  continuity cannot be maintained and/or restored in accordance with the requirements of the business, then the business will not experience the value that has been promised. Without continuity, the utility (fitness for use) of the service cannot be accessed.

第 5 章角色职责: Leads the team and facilitates, this role is the same as “Scrum Master” in Scrum.Implements visual control across the entire process and has a strong focus on establishing a stream-lined process with one-piece flow.

第5部分:devOps Team Roles (Leads the team and facilitates, this role is the same as “Scrum Master” in Scrum. Implements visual control across the entire process and has a strong focus on establishing a stream-lined process with one-piece flow.) 

第5部分:Gatekeeper / Release coordinator: Responsible for  monitoring the operational status and progress of the next release of the IT service. Make go/no go decisions about deployment according to criteria including security, compliance, regulatory requirements, maturity of operation team and their process views.

第7部分:devOps Process Project Planning The service master creates the vision, goal, and value of the project, and then puts together the devOps team members. 

第 7 章 JKK 解释：它基于 100％完成高质量项目创建完成定义

第7部分:devOps Process Obeya is a war room which serves two purposes - information management and on-the-spot decision making.

第8部分 devOps implementation collaboration: (Standard): This focuses on just providing quick and frequent IT services and reliable operation and is led by the service  master. It is most suited for SoE and SoR 

dEVOPS团队由 6-8 名成员组成的跨职能团队。

The Service Master EOL

精益管理中的八大浪费类型

持续交付第11章基础设施和环境管理 225页：自动化的准备工作与自动化维护相结合，可保证出现问题就能在可预见的时间内重建基础设施。

持续交付第11章 11.7.2章节 虚拟环境和部署流水线。250页 构建和发布管理系统应该能记住用来运行部署流水线的虚拟机模板，当部署到生产环境时，也应该能够启动同一套虚拟机模板。

持续交付第四章 测试策略的实现 71页当你有时间写更多的自动化测试时，很难在Happy Path和Sad Path之间进行选择。 如果你的应用程序比较稳定，那么Happy Path应该是你的首选，因为它们是用户所定义的场景。

持续交付第4.3章节 75页制定遵守INVEST原则[即独立的（Independent）、可协商的（Negotiable）、有价 值的（Valuable）、可估计的（Estimable）、小的（Small）且可测试的（Testable）] 的用户故事[ddVMFH]及考虑其验收条件。

持续交付第15章 持续交付管理：340页企业治理更关注于符合度 （conformance），即遵从性、保障、监管、责任和透明管理， 而业务治理（business governance）更关注业务和价值创造的执行度（performance）。其实，执行度和符 合度都可以满足。这一道理在持续交付中也是正确的。通过确保交付团队能得到应用 程序在类生产环境上的不断反馈， 是部署流水线达成“执行度”这个目标的方法和手段。部署流水线使交付流程更加透明，来帮助团队达成符合度

持续交付 5.4 提交阶段 97页

15.3章节 344页：我们的关注点在于迭代和增量交付，以及跨功能职责角色之间的协 作。345页：Scrum是一种迭代式增量软件开发过程，通常用于敏捷软件开发。包括一系列 实践和预定义角色的过程框架。347页 开发与发布：我们推荐以迭代增量式过程进行软件 的开发与发布。

第3章 持续集成 44页 高效使用持续集成 的那些团队能够比那些没有使用它的团队更 快地交付软件，且缺陷更少。在交付过程 中，缺陷被发现得越早，修复它的成本就越低， 因此也就大大节省了成本和时间。因 此我们认为，对于专业的软件交付团队来说，持续集成与版本控制同等重要。

持续交付 3.7 章节 60 页从技术角度上看，最为简单的方法（也是从流程角度上讲最有效的方法）就是使 用共享的版本控制系统和持续集成系统。 63 页 3.8 章节：，dVcS 引入了一个中间层：在本地工作区的修改必须先提交到本地库，然后才能 推送到其他仓库，而更新本地工作区时，必须先从其他仓库中将代码更新到本地库。以及 14.4 章节

持续交付第五章。什么不是部署管道的一部分？88页

6.2 章节 构建工具：117 页 各构建工具的不同点在于它是任务导向的，还是产品导向的。任务导向的 构建工具（比如 ant、Nant 和 MSbuild）会依据一系列的任务描述依赖网络， 而产品导 向的工具，比如 Make（创建可执行文件），是根据它们生成的产物（比如一个可执行文件）来描述。

项目失败：转向持续部署模式的最佳第一步是什么？开发应非正式地涉及整个项目的运维，并合作共同创建部署脚本。

哪种做法有助于在不影响质量的情况下提高速度？One-piece-flow

手动软件发布过程的优点是什么？鼓励协作，团队成员则审查彼此的工作。

第八章：自动化验收测试 154 页：首先，由于反馈环将 大大缩短，缺陷被发现得更快，也就更容易修复。其次，由于测试人员、开发人员和 客户需要紧密合作才能创建一个良好的自动化测试套件，这会促进他们之间的良好合 作，而且每个人都将关注软件应该交付的业务价值。

第 3 章，3.5.1 52 页、53 页  你的同事说：“即使构建中断，你也应该check In。

老板问实施devOps，What would not be a good reason? 团队解释了新实践在另一家公司中的运作方式。

8.5.1 验收测试中的状态 p167首先，要抵制使用生产数据的备份作为验收测试的测试数据库的诱惑（尽管有时 它对吞吐量测试是有用的）。相反，我们要维护一个受控的数据最小集。测试的一个关 键方面是建立一个确知的起始点。通过生产数据的副本以进行测试。

12.4 章节 数据库回滚和无停机发布 P268

11.9 基础设施和应用程序的监控 p257 收集数据、记录日志、建立信息展示板、行为驱动监控。提出了四种可能的操作来排除应用程序故障 1,2&4

13.5.5 循环依赖。 P302 循环依赖构建阶梯

13.3 依赖 P286 构建时依赖与运行时依赖之间的区别如下：构建时依赖会出现在应用程序编译和链接时（如果需要编译和链接的话）；而运行时依赖会出现在应用程序运行并完成它的某些操作时。

第 2 章 配置管理 P24 进行增量更改，满足我所在组织的所有合规性规定

第 11 章 基础设施管理 P224  第 11 章 基础设施管理 P224
准备部署环境的过程以及部署之后对环境的管理是本章的主要内容。然而为了能 够做到这一点，就要基于下面这些原则，用一个整体方法来管理所有基础设施。 ①   使用保存于版本控制库中的配置信息来指定基础设施所处的状态。  基础设施应该具有自治特性，即它应该自动地将自己设定为所需状态。  通过测试设备和监控手段，应该每时每刻都能掌握基础设施的实时状况。 什么不是管理基础设施的原则？编写新的通用基础管理脚本。

2.2 章版本控制 P27  什么不被认为是实现配置管理这一目标的有效策略？始终将二进制文件和配置信息保存在一起。

11.8.1 P254  cloud 安全和服务水平

11.8.2 云中平台 P255 将应用部署到完全标准化的应用栈上，就意味着不需要担心测试环境、试运行 环境和生产环境的配置和维护，也不需要担心虚拟机映像的管理。最后一点尤其是革命性的。在本书中，用了大量的篇幅来讨论如何自动化你的部 署、测试和发布流程，以及如何搭建和管理测试和部署环境。云中的哪个平台特性最能实现自动部署？ Standardized stack

10.5 紧急修复 P216  任何情况下，都不能破坏流程。紧急修复版本 也要走同样的构建、部署、测试和发布流程，与其他代码变更没什么区别。发布 RFc 作为紧急补丁并通知所有利益相关者。 然后根据 SLa 应用紧急补丁。P216

---
高效的 devOps 文化需要具备：高效沟通，共识与信任，人性化的员工配置和资源
写一份项目章程很重要，一起写项目范围最好的理由是什么：确保所有团队对成功有着相同的定义
衡量已找到程序错误的总数是跟踪项目质量最好的方法吗：全局范围内而非局部进行优化
服务级别协议对专注于増加商业价值有所帮助

---
1. 场景：5 个月 h 后……，开发完成后才向类生产环境部署（考点，反模式）
2. 在发布流程为软件发布创建一个可重复且可靠的过程。（考点，软件交付原则）
3. dEVOPS 应该建立一个 Pull System、One Piece Flow 的 IT Super chain
4. 建立 dEVOPS New Mind of all People，流程 One Piece Flow
5. 场景：一家公司由于供应商的要求，只剩下 5 个月的时间建立 dEVOPS 的团队，他 们如何决定他们的迭代时间和频次？ 根据符合业务要求来确定
6. dEVOPS 四个支柱 collaboration，Multiple People
7. 要进行 dEVOPS 转型，coach Team 中的每一个人，建立 blameless 的文化 
8. devOps 转型：沟通、同理心、人员和资源
9. 场景：cEO 以前总是骂下面的人，现在采用了 dEVOPS 方式，情况有所好转，但是 没有达到理想效果，应该：Trust each Other，建立 blameless 文化
10. 一个人抱怨另一个人，通过邮件到处宣扬对另一个人的不满，avoidance
11. Gatekeeper 的职责：Security、compliance、Regulatory Requirements
12. 浪费的类型：Extra Feature
13. 在 dEVOPS 项目计划时，SLR 包含那些内容、安全、连续性等内容
14. dEVOPS 项目开始时要做那些工作？Vision、Goal 等
15. SOR 适用月那种 dEVOPS 方式，collaboration
16. Obeya，信息共享，做决定
17. auto Provisioning  auto maintenance
18. VM 模板
19. SLa 的重要性，业务连续性
20. 在稳定没有时间的情况下进行：Happy Path 测试
21. 场景：销售人员提出了一些列销售问题，这个用户故事符合 INVEST 原则？问选哪 一个：可测试性
22. development Pipeline 对治理的作用
23. 提交阶段需要做什么工作？
24. 一个团队三个月进行一次新版本的部署，而且是按照模块开发和部署，因此他们认 为他们的采用的是 dEVOPS 的方法，选项问适合否
25. 持续集成的优势 fewer bug、cheaper
26. check In polices
27. Run time build time
28. 安全问题出现后，如何进行紧急变更
29. 谁可以决定服务终止
30. 数据库管理，此题是技术题，大概内容是要做一个回退脚本，且要保证原有数据不 能删除，但可能重构表内容。
31. 不上云的理由，安全，SLa
32. Standardized stack
33. 手工发布软件的优势
34. 手工测试的类型
35. 场景：某人要提交有问题的代码，且振振有词讲到提交后可能通过变更进行更正。 
36. 循环依赖
37. Ensures performance (fast delivery) and conformance (to requirements
38. 不要将生产数据库拿到测试环境
39. JKK 100%质量覆盖
40. 版本控制，巴黎，伦敦，悉尼，还有其他地方存在时差，如何进行版本控制
41. 基础架构环境准备
42. cross Function 的 6-8 人
43. 不要把二进制文件放进构件库中
44. Make such as an Executable
45. Product Oriented
46. Process Master 的职责，建立可视化看板，单件流等 
47. 配置管理策略，增量，合规
48. Increase speed without compromising quality


