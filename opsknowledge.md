
# 运维知识体系

2020-10

![img](imgs/yunweitixi.png)

## 网络
- DNS
- CDN
- 防火墙、路由器、Ipsec VPN、链路负载均衡和高可用 （CCNP级别）
- 三层交换 动态路由（OSPF）、静态路由、EC（端口汇聚）、MSTP+VRRP等 （CCNP级别）
- OVS
- TCPIP
    - 三次握手、四次挥手、状态转换、TCP队列
## 高可用
- 架构
- 服务降级、异地灾备、智能监控
## 监控
- 智能监控
- zabbix
- granfana
- prometheus
## 知识库
## 上线
- 灰度
    - 金丝雀
    - AB
    - 蓝绿
- 规范
    - 流程
    - 交叉检查/checklist
    - 备用方案
    - 回滚方案
## 存储
- 块存储
    - 机械硬盘、SSD、文件系统（ext4、xfs）、LVM、tmpfs
- 文件分发（多级分发）、文件同步（rsync、inotify）、DRBD、DAS（块存储）
- 文件存储
    - NFS（Unix/Linux）、FTP、SAN、iSCSI
- 分布式存储
    - 对象存储
    - GlusterFS、MooseFS、Ceph、FastDFS（非对象存储）
## 负载均衡
- 四层
    - 开源：LVS（IP负载均衡）+Keepalived、Haproxy 商业：F5、Netscaler
- 七层/SLB
    - 反向代理：Haproxy、Nginx、Apache（根据HTTP协议支持的属性进行L7分发）、A/B Test Gateway、WAF
## 数据库
- 数据访问
    - 应用层分片、淘宝TDDL、开源：360（Atlas）、阿里（Cobar）、MyCat、MySQL-Proxy、根据业务开发
- 分布式缓存
    - Memcached、Redis（客户端分片、Redis Cluster、Twemproxy、Codis）
- NoSQL
    - Redis、LevelDB（SSDB）、CouchDB、Mongodb、Couchbase 、Cassandra、TiDB（支持MySQL协议）
- 时间序列DB
    - RRDTool、Graphite Whisper、OpenTSDB、InfluxDB、KairosDB、ElasticSearch、Hbase
- RDBMS
    - MySQL（PXC集群、MHA）、Oracle（DG、OGG、RAC）、PostgreSQL、SqlServer、SQLite、DB2
- 大数据
    - Hadoop生态圈（HDFS、Hive、Hbase、Zookeeper、Pig、Spark、Impala、Kudu）、Mahout智能推荐
## web服务
- HTTP协议、Web服务器（Apache、Nginx/OpenResty、Tomcat、Resin、Jboss）安全设置、性能优化
- 配置管理
    - SaltStack
- 服务框架
    - SOA框架（Dubbo）、微服务框架（istio、Spring Cloud）、协议（RPC、RESTful）、框架安全、应用性能监控
## 反向代理
- ATS、Squid、Varnish、Nginx(缓存分级、预缓存、缓存刷新）
## 云原生
- 公有云、私有云（OpenStack/cloudstack+KVM/XEN、oVirt）、混合云
- 容器化技术
- 限流
- serverless
- 工具集
- 编排管理
- 注册中心/服务治理
- 基础理论：不可变基础设施/编排技术
## devops
- 价值观
- CI/CD
- 工具集
## SDN技术栈
## 运维管理
- 变更管理
- 故障管理
- 工单管理
- 作业巡检
- 应急处理
## 运维开发
- 脚本语言
    - AWK、Sed、Shell、Python、PHP、Perl、Ruby、Lua
## 消息队列
- kafka
- rabbitMQ
## 日志采集
- flume，Flume的日志源可以是mysql数据库
- 较成熟的日志系统方案ELK：Filebeat（采集） → Logstash（日志解析） → ElasticSearch（日志分析） → Kibana（可视化）
  filebeat采集后可放入kafka（ELKK）

## 自动化运维
1. 运维需求，需求工单化，可追踪回溯
2. 部署自动化要求
3. 资产清单：虚拟机、物理机、k8s集群、网络设备，容器？
4. 资产管理：虚拟机添加，资源统计，虚拟机初始化;节点管理
5. 架构介绍：整体架构, 边缘网关,任务放在zookeeper，teamwork客户端监听是否有任务任务放入kafka

服务批量升级能力
helm, ansible
脚本管理: 执行目标，应用，主机，项目，节点等;
          支撑操作（如取数等）, 与运维接解耦

<<<<<<< HEAD

## 20220323 自动化运维分享
视角
=======
## 视角
>>>>>>> 56aa8c4ec6ed1ca59fa54b190fa87534fbdfdd35
分享，提炼，表达，理解，清晰，业务
自动化运维，对标;专业;产品思维;解放双手，有更多时间思考
运维系统性思考，监控，部署，稳定性，投入;
提出一些挑战，白天能不能操作?
自动化 - 智能化(发现-特征-判断-自愈)
工作方式上，规范 - 总结

<<<<<<< HEAD
## 20220324 go分享
项目结构

=======
## 其他
- [关于运维研发协作的问题](opsquestion.md)
>>>>>>> 56aa8c4ec6ed1ca59fa54b190fa87534fbdfdd35
